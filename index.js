import express from "express";
import cors from "cors";

import UserRoute from "./routes/UserRoute.js";
import AuthRoute from "./routes/AuthRoute.js";

import dotenv from "dotenv"; 
import cookieParser from "cookie-parser";
import bodyParser from "body-parser"
dotenv.config();

var app = express()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())
 
app.use(cors({ credentials:true, origin:'http://localhost:3000' }));

app.use(cookieParser());

app.use(express.json());
app.use(UserRoute,AuthRoute);
 
app.listen(5000, ()=> console.log('Server up and running...'));