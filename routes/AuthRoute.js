import express from "express";
import { verifyToken } from "../middleware/VerifyToken.js";
import {
	Register,
	Login,
	refreshToken,
	Logout
} from "../controllers/AuthController.js";

const router = express.Router();

router.post('/register', Register);
router.post('/login', Login);
router.get('/token', refreshToken);
router.post('/logout', Logout);
export default router;